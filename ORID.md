# O

- Concept Map of Spring Boot
- Html, CSS
- React

# R

I am frustrated.

# I

I am not familiar with the syntax of React, and I often need to consult the documentation when writing code. Meanwhile, the shortcut keys of vsCode are very different from idea, which slows down the writing speed of code.

# D

I will do more practice.